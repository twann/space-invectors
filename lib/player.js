/** 
  Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
  Copyright (C) 2022-2025 Twann <tw4nn@disroot.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { getArenaPosition, isEmptyPosition, arenaDimensions } from "./utils.js";

export class Player {
  /**
   * Creates an instance of Player.
   * @export
   * @param {string} playerId
   * @param {number} posRow
   * @param {number} posCol
   * @memberof Player
   */
  constructor(playerId, posRow, posCol) {
    this.id = playerId;
    this.lives = 5;
    this.shields = 0;
    this.posRow = posRow;
    this.posCol = posCol;
    this.cheatUsed = false;
    this.elementDOM = document.createElement("img");
    this.elementDOM.src = "./svg/player_ship.svg";
    this.elementDOM.id = this.id;
    this.elementDOM.classList.add("player");
    this.updatePosition();
  }

  /**
   * Get active Player entity.
   * Since only one Player entity can exist at a time, the entity ID is not needed.
   * @static
   * @return {Player}
   * @memberof Player
   */
  static get() {
    return document.game.player;
  }

  /**
   * Update the position of the entity in the arena.
   * @memberof Player
   */
  updatePosition() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    getArenaPosition(this.posRow, this.posCol).appendChild(this.elementDOM);
  }

  /**
   * Spawn a PlayerShot entity on the same row and column as this entity.
   * @return {PlayerShot}
   * @memberof Player
   */
  shoot() {
    if (isEmptyPosition(this.posRow, this.posCol)) {
      const shotId = "shot-" + document.game.playerShotsTotal;
      const shot = new PlayerShot(shotId, this.posRow, this.posCol);
      document.game.playerShots[shotId] = shot;
      return shot;
    }
  }

  /**
   * Move the entity one column to the left.
   * @memberof Player
   */
  moveLeft() {
    const dimensions = arenaDimensions();
    if (this.posCol == 1) {
      this.posCol = dimensions.cols;
    } else {
      this.posCol -= 1;
    }
    this.updatePosition();
  }

  /**
   * Move the entity one column to the right.
   * @memberof Player
   */
  moveRight() {
    const dimensions = arenaDimensions();
    if (this.posCol == dimensions.cols) {
      this.posCol = 1;
    } else {
      this.posCol += 1;
    }
    this.updatePosition();
  }

  /**
   * Explode Player entity and make it loose one life.
   * If the Player entity reaches zero lives, the game will end.
   * Otherwise, a shield of 50 cycles will be granted to the user.
   * @return {null}
   * @memberof Player
   */
  die() {
    if (this.shields !== 0) {
      // we do not check if it is lower than 0 so that cheats work
      return null;
    }
    this.lives -= 1;
    this.elementDOM.src = "./svg/explode.svg";
    document.querySelector("#arena--table").classList.add("red");
    if (this.lives === 0) {
      document.game.isEnded = true;
    } else {
      this.shields = 50;
    }
  }
}

export class PlayerShot {
  /**
   * Creates an instance of PlayerShot.
   * @export
   * @param {string} shotId
   * @param {number} posRow
   * @param {number} posCol
   * @memberof PlayerShot
   */
  constructor(shotId, posRow, posCol) {
    this.id = shotId;
    this.posRow = posRow;
    this.posCol = posCol;
    this.elementDOM = document.createElement("img");
    this.elementDOM.src = playerShotImage();
    this.elementDOM.id = this.id;
    this.elementDOM.classList.add("player-shot");
    document.game.playerShotsTotal += 1;
    this.updatePosition();
  }

  /**
   * Get PlayerShot entity with given ID.
   * @static
   * @param {string} shotId
   * @return {PlayerShot}
   * @memberof PlayerShot
   */
  static get(shotId) {
    return document.game.playerShots[shotId];
  }

  /**
   * Permanently delete entity.
   * @memberof PlayerShot
   */
  die() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    delete document.game.playerShots[this.id];
  }

  /**
   * Update the position of the entity in the arena.
   * @memberof PlayerShot
   */
  updatePosition() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    getArenaPosition(this.posRow, this.posCol).appendChild(this.elementDOM);
  }

  /**
   * Move entity one row up.
   * If the entity is on the first row, it will die instead.
   * If the entity hits an Alien entity, it will kill the latter and then die.
   * @return {null}
   * @memberof PlayerShot
   */
  moveUp() {
    if (this.dead) {
      return null;
    }
    let killedAlien = false;
    if (this.posRow === 1) {
      this.die();
      return null;
    } else if (!isEmptyPosition(this.posRow - 1, this.posCol)) {
      for (const child of getArenaPosition(this.posRow - 1, this.posCol)
        .children) {
        if (child.classList.contains("alien")) {
          const alienToKill = document.game.aliens[child.id];
          alienToKill.die();
          killedAlien = true;
        }
      }
    }
    if (killedAlien) {
      this.die();
    } else {
      this.posRow -= 1;
      this.updatePosition();
    }
  }
}

/**
 * Return the player shot icon to use based on the number of aliens killed.
 * @return {string}
 */
function playerShotImage() {
  if (document.game.stats.aliensKilled >= 1000) {
    return "./svg/shot7.svg";
  } else if (document.game.stats.aliensKilled >= 500) {
    return "./svg/shot6.svg";
  } else if (document.game.stats.aliensKilled >= 200) {
    return "./svg/shot5.svg";
  } else if (document.game.stats.aliensKilled >= 100) {
    return "./svg/shot4.svg";
  } else if (document.game.stats.aliensKilled >= 50) {
    return "./svg/shot3.svg";
  } else if (document.game.stats.aliensKilled >= 30) {
    return "./svg/shot2.svg";
  } else if (document.game.stats.aliensKilled >= 10) {
    return "./svg/shot1.svg";
  } else {
    return "./svg/shot0.svg";
  }
}
