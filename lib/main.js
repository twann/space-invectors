/** 
  Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
  Copyright (C) 2022-2025 Twann <tw4nn@disroot.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { startGame } from "./game.js";
import { closeAllSections } from "./utils.js";

/**
 * Open initial game introduction section, that contains general tips and keyboard navigation instructions.
 */
export function openIntro() {
  closeAllSections();
  const sectionIntro = document.querySelector("section#game-intro");
  sectionIntro.classList.add("active");
}

/**
 * Open game lobby section (that is displayed when the page is loaded).
 */
export function openLobby() {
  closeAllSections();
  const sectionLobby = document.querySelector("section#start-lobby");
  sectionLobby.classList.add("active");
}

/**
 * Open game arena section and start the game.
 */
export function openArena() {
  closeAllSections();
  const sectionArena = document.querySelector("section#game-arena");
  sectionArena.classList.add("active");
  startGame();
}
