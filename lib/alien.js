/** 
  Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
  Copyright (C) 2022-2025 Twann <tw4nn@disroot.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import {
  getArenaPosition,
  isEmptyPosition,
  arenaDimensions,
  randomChoice,
} from "./utils.js";

export class Alien {
  /**
   * Creates an instance of Alien.
   * @export
   * @param {string} alienId
   * @param {number} posRow
   * @param {number} posCol
   * @param {string} color
   * @memberof Alien
   */
  constructor(alienId, posRow, posCol, color) {
    this.id = alienId;
    this.posRow = posRow;
    this.posCol = posCol;
    this.color = color;
    this.dead = false;
    this.elementDOM = document.createElement("img");
    switch (color) {
      case "green":
        this.elementDOM.src = "./svg/alien_green.svg";
        break;
      case "blue":
        this.elementDOM.src = "./svg/alien_blue.svg";
        break;
      case "red":
        this.elementDOM.src = "./svg/alien_red.svg";
        break;
      default:
        this.elementDOM.src = "./svg/alien_green.svg";
        break;
    }
    this.elementDOM.id = this.id;
    this.elementDOM.classList.add("alien");
    document.game.aliensTotal += 1;
    this.updatePosition();
  }

  /**
   * Update the position of the entity in the arena.
   * @memberof Alien
   */
  updatePosition() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    getArenaPosition(this.posRow, this.posCol).appendChild(this.elementDOM);
  }

  /**
   * Spawn an AlienShot entity one row below this entity.
   * @return {null}
   * @memberof Alien
   */
  shoot() {
    if (this.dead) {
      return null;
    }
    if (
      this.posRow + 1 === document.game.player.posRow &&
      this.posCol === document.game.player.posCol
    ) {
      document.game.player.die();
    } else {
      const shotId = "alien-shot-" + document.game.alienShotsTotal;
      const shot = new AlienShot(shotId, this.posRow + 1, this.posCol);
      document.game.alienShots[shotId] = shot;
    }
  }

  /**
   * Move the entity one column to the left.
   * @return {null}
   * @memberof Alien
   */
  moveLeft() {
    if (this.dead) {
      return null;
    }
    const dimensions = arenaDimensions();
    if (this.posCol === 1) {
      if (isEmptyPosition(this.posRow, dimensions.cols)) {
        this.posCol = dimensions.cols;
        this.updatePosition();
      }
    } else if (isEmptyPosition(this.posRow, this.posCol - 1)) {
      this.posCol -= 1;
      this.updatePosition();
    }
  }

  /**
   * Move the entity one column to the right.
   * @return {null}
   * @memberof Alien
   */
  moveRight() {
    if (this.dead) {
      return null;
    }
    if (this.posCol === arenaDimensions().cols) {
      if (isEmptyPosition(this.posRow, 1)) {
        this.posCol = 1;
        this.updatePosition();
      }
    } else if (isEmptyPosition(this.posRow, this.posCol + 1)) {
      this.posCol += 1;
      this.updatePosition();
    }
  }

  /**
   * Move the entity one row below.
   * @return {null}
   * @memberof Alien
   */
  moveDown() {
    if (this.dead) {
      return null;
    }
    if (isEmptyPosition(this.posRow + 1, this.posCol)) {
      this.posRow += 1;
      this.updatePosition();
      if (this.posRow === arenaDimensions().rows) {
        document.game.isEnded = true;
      }
    }
  }

  /**
   * Explode Alien entity and mark it as deleted.
   * Alien.cleanDead() should be called during the next cycle to delete this entity completely.
   * @memberof Alien
   */
  die() {
    document.game.cycleDelay -= 0.2;
    this.elementDOM.src = "./svg/explode.svg";
    this.dead = true;
    document.game.stats.aliensKilled += 1;
  }

  /**
   * Get Alien entity with given entity ID.
   * @static
   * @param {string} alienId
   * @return {Alien}
   * @memberof Alien
   */
  static get(alienId) {
    return document.game.aliens[alienId];
  }

  /**
   * Spawn new Alien entity in given column of the first row.
   * @static
   * @param {number} column
   * @return {Alien}
   * @memberof Alien
   */
  static spawn(column) {
    const alienColor = randomChoice(["green", "blue", "red"]);
    const alienId = "alien-" + document.game.aliensTotal;
    const alien = new Alien(alienId, 1, column, alienColor);
    document.game.aliens[alienId] = alien;
    return alien;
  }

  /**
   * Spawn new Alien entities in every column of the first row.
   * @static
   * @memberof Alien
   */
  static spawnRow() {
    document.game.cycleDelay -= 1;
    for (const column of Array(arenaDimensions().cols).keys()) {
      Alien.spawn(column + 1);
    }
  }

  /**
   * Remove all exploded Alien entities.
   * @static
   * @memberof Alien
   */
  static cleanDead() {
    for (const alienId in document.game.aliens) {
      const alien = document.game.aliens[alienId];
      if (alien.dead && alien.elementDOM.parentElement !== null) {
        alien.elementDOM.parentElement.removeChild(alien.elementDOM);
        delete document.game.aliens[alienId];
      }
    }
  }
}

export class AlienShot {
  /**
   * Creates an instance of AlienShot.
   * @export
   * @param {string} shotId
   * @param {number} posRow
   * @param {number} posCol
   * @memberof AlienShot
   */
  constructor(shotId, posRow, posCol) {
    this.id = shotId;
    this.posRow = posRow;
    this.posCol = posCol;
    this.elementDOM = document.createElement("img");
    this.elementDOM.src = "./svg/alien_shot.svg";
    this.elementDOM.id = this.id;
    this.elementDOM.classList.add("alien-shot");
    document.game.alienShotsTotal += 1;
    this.updatePosition();
  }

  /**
   * Get AlienShot entity with given ID.
   * @static
   * @param {string} shotId
   * @return {AlienShot}
   * @memberof AlienShot
   */
  static get(shotId) {
    return document.game.alienShots[shotId];
  }

  /**
   * Permanently delete entity.
   * @memberof AlienShot
   */
  die() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    delete document.game.alienShots[this.id];
  }

  /**
   * Update the position of the entity in the arena.
   * @memberof AlienShot
   */
  updatePosition() {
    if (this.elementDOM.parentElement !== null) {
      this.elementDOM.parentElement.removeChild(this.elementDOM);
    }
    getArenaPosition(this.posRow, this.posCol).appendChild(this.elementDOM);
  }

  /**
   * Move the entity one row below.
   * If the entity is on the last row, it will die instead.
   * If the entity hits the player, it will kill the latter and then die.
   * @return {null}
   * @memberof AlienShot
   */
  moveDown() {
    if (this.dead) {
      return null;
    }
    if (this.posRow === arenaDimensions().rows) {
      this.die();
    } else if (
      this.posRow + 1 === document.game.player.posRow &&
      this.posCol === document.game.player.posCol
    ) {
      document.game.player.die();
      this.die();
    } else {
      this.posRow += 1;
      this.updatePosition();
    }
  }
}
