/** 
  Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
  Copyright (C) 2022-2025 Twann <tw4nn@disroot.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/**
 * Get the DOM element cell corresponding to the specified arena position.
 * @export
 * @param {number} posRow
 * @param {number} posCol
 * @return {Element}
 */
export function getArenaPosition(posRow, posCol) {
  return document.querySelector(
    "#arena--table .arena.row-" + posRow + " .arena.col-" + posCol,
  );
}

/**
 * Checks whether the cell corresponding to the specified arena position is empty.
 * @export
 * @param {number} posRow
 * @param {number} posCol
 * @return {boolean}
 */
export function isEmptyPosition(posRow, posCol) {
  const position = getArenaPosition(posRow, posCol);
  if (position.childElementCount > 0) {
    return (
      position.childElementCount == 1 &&
      position.children[0].classList.contains("player")
    );
  } else {
    return true;
  }
}

/**
 * Get the number of rows and columns of the arena.
 * @export
 * @return {object}
 */
export function arenaDimensions() {
  const rows = document.querySelectorAll("tr.arena").length;
  const cols = document.querySelectorAll("tr.arena.row-1 td.arena").length;
  return {
    rows: rows,
    cols: cols,
  };
}

/**
 * Return a random value from given array.
 * @export
 * @param {Array} customArray
 * @return {*}
 */
export function randomChoice(customArray) {
  return customArray[Math.floor(customArray.length * Math.random())];
}

/**
 * Close all section of the HTML user interface.
 * This should never be called directly, since it will result in a blank page.
 * @export
 */
export function closeAllSections() {
  for (const section of document.getElementsByTagName("section")) {
    section.classList.remove("active");
  }
}
