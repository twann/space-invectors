/** 
  Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
  Copyright (C) 2022-2025 Twann <tw4nn@disroot.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { Alien } from "./alien.js";
import { Player } from "./player.js";
import { arenaDimensions, closeAllSections } from "./utils.js";

document.game = {
  aliens: {},
  aliensTotal: 0,
  alienShots: {},
  alienShotsTotal: 0,
  playerShots: {},
  playerShotsTotal: 0,
  player: null,
  isEnded: false,
  cycleDelay: 151, // we set it at 151 because the initial alien row spawn will remove 1
  stats: {
    aliensKilled: 0,
    cycles: 0,
  },
};
let timer;

/**
 * Perform the required actions to make the game work before going onto the next cycle, i.e.:
 *   0. Stop the game if still running
 *   1. Remove dead entities
 *   2. Update player shots = move up AND kill aliens
 *   3. Update alien shots = move down AND hurt player
 *   4. Update aliens = move (down/left/right) or shoot
 *   5. Optionally spawn new aliens if there are no longer any
 *   6. Weaken shield if player has one
 *   7. Update player information in UI
 *   8. Update cycle counts
 *   9. Proceed to the next cycle, using the updated inter-cycle delay
 * @return {null}
 */
function processCycle() {
  if (document.game.isEnded) {
    clearTimeout(timer);
    timer = null;
    console.info("Game ended");
    document.removeEventListener("keypress", keyboardNavigation);
    calculateStats();
    timer = setTimeout(openStats, 1500);
    return null;
  }

  const arena = document.querySelector("#arena--table");
  if (arena.classList.contains("red")) {
    arena.classList.remove("red");
  }

  Alien.cleanDead();
  for (const playerShotId in document.game.playerShots) {
    const playerShot = document.game.playerShots[playerShotId];
    playerShot.moveUp();
  }
  for (const alienShotId in document.game.alienShots) {
    const alienShot = document.game.alienShots[alienShotId];
    alienShot.moveDown();
  }
  let someAliensAreAlive = false;
  for (const alienId in document.game.aliens) {
    someAliensAreAlive = true;
    const alien = document.game.aliens[alienId];
    const prob = 100 * Math.random();
    if (prob < 2.5) {
      alien.shoot();
    } else if (prob < 4.5) {
      alien.moveDown();
    } else if (prob < 5.5) {
      alien.moveLeft();
    } else if (prob < 6.5) {
      alien.moveRight();
    }
  }
  if (!someAliensAreAlive) {
    Alien.spawnRow();
  }
  if (document.game.player.shields > 0) {
    document.game.player.shields -= 1;
    if (document.game.player.shields > 0) {
      document.game.player.elementDOM.src = "./svg/player_ship_protected.svg";
    } else {
      document.game.player.elementDOM.src = "./svg/player_ship.svg";
    }
  }
  document.querySelector("#lives .value").textContent =
    document.game.player.lives;
  document.querySelector("#shields .value").textContent =
    document.game.player.shields;
  document.querySelector("#fired .value").textContent =
    document.game.playerShotsTotal;
  document.querySelector("#kills .value").textContent =
    document.game.stats.aliensKilled;
  document.querySelector("#speed .value").textContent = (
    150 / document.game.cycleDelay
  ).toFixed(2);
  document.game.stats.cycles += 1;
  timer = setTimeout(processCycle, document.game.cycleDelay);
}

/**
 * Handler for `keypress` events.
 * Without this, the player won't be able to do anything.
 * @param {Event} event
 */
const keyboardNavigation = (event) => {
  switch (event.key) {
    case "j":
      if (timer !== null) {
        document.game.player.moveLeft();
      }
      break;

    case "k":
      if (timer !== null) {
        document.game.player.moveRight();
      }
      break;

    case " ":
      if (timer !== null) {
        document.game.player.shoot();
      }
      break;

    case "*":
      cheatInvincible();
      break;

    case "q":
      clearTimeout(timer);
      timer = null;
      break;

    case "Enter":
      if (timer === null) {
        processCycle();
      }
      break;

    default:
      console.log(event);
      break;
  }
};

/**
 * Initial function that starts the document.game.
 * This will bind `keypress` events to the handler function defined above.
 * @export
 */
export function startGame() {
  buildArena();
  const arenaDims = arenaDimensions();
  const initialPos = {
    row: arenaDims.rows,
    col: Math.floor(arenaDims.cols / 2),
  };
  document.game.player = new Player("player-0", initialPos.row, initialPos.col);
  document.addEventListener("keypress", keyboardNavigation);
  processCycle();
}

/**
 * Enable cheat that makes the player invincible.
 */
function cheatInvincible() {
  const player = Player.get();
  player.shields = -1;
  player.elementDOM.src = "./svg/player_ship_cheat.svg";
  player.cheatUsed = true;
}

/**
 * Calculate end-of-game stats and siplay them in the UI.
 */
function calculateStats() {
  const player = Player.get();
  let accuracy = 0;
  if (document.game.playerShotsTotal > 0) {
    accuracy =
      document.game.stats.aliensKilled / document.game.playerShotsTotal;
  }
  let slowness = document.game.stats.cycles;
  if (document.game.stats.aliensKilled > 0) {
    slowness = document.game.stats.cycles / document.game.stats.aliensKilled;
  }
  const playerScore = getScore();
  document.querySelector("#stats-score .value").textContent = playerScore.toFixed(3);
  document.querySelector("#stats-cycles .value").textContent =
    document.game.stats.cycles;
  document.querySelector("#stats-kills .value").textContent =
    document.game.stats.aliensKilled;
  document.querySelector("#stats-fired .value").textContent =
    document.game.playerShotsTotal;
  document.querySelector("#stats-accuracy .value").textContent =
    (100 * accuracy).toFixed(3) + "%";
  document.querySelector("#stats-speed .value").textContent =
    slowness.toFixed(3);
}

/**
 * Calculate score based on player stats and other metrics.
 * @return {number}
 */
function getScore() {
  const accuracy = document.game.stats.aliensKilled / document.game.playerShotsTotal;
  let score = 0;
  if (document.game.player.cheatUsed) {
    return score;
  }
  if (document.game.player.lives == 0) {
    score += 20;
  }
  score += (20 * accuracy);
  score += (.05 * document.game.stats.aliensKilled);
  score += (10 * (document.game.stats.aliensKilled / document.game.stats.cycles));
  return score;
}

/**
 * Open end-of-game menu with player score and statistics.
 */
function openStats() {
  closeAllSections();
  const sectionStats = document.querySelector("section#end-menu");
  sectionStats.classList.add("active");
}

/**
 * Build initial arena.
 * The primary goal of this function is to make the HTML file smaller.
 * TODO: In the future, it could also be used to dynamically adapt the arena size to the window dimensions.
 */
function buildArena() {
  const arena = document.getElementById("arena--table");
  for (const rowNum of Array(16).keys()) {
    const row = document.createElement("tr");
    row.classList.add("arena");
    row.classList.add("row-" + (rowNum + 1));
    for (const colNum of Array(20).keys()) {
      const col = document.createElement("td");
      col.classList.add("arena");
      col.classList.add("col-" + (colNum + 1));
      row.appendChild(col);
    }
    arena.appendChild(row);
  }
}
