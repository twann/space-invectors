<p align="center">
<img src="svg/icon.svg" width="100px" height="100px">
</p>
<h1 align="center">Space Invectors</h1>
<p align="center">
An AGPLv3-licensed "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
</p>

## Usage

Clone the repository and play it completely offline, simply by starting a local HTTP server, serving the repository root and opening it in a web browser. You can do so by running:

```bash
# clone repository
git clone https://codeberg.org/twann/space-invectors.git space-invectors

# enter repository root
cd space-invectors

# start local HTTP server
python3 -m http.server
```

Alternatively, you can try the [live demo here](https://twann.codeberg.page/space-invectors).

## About

The purpose of this project is a fun way for myself (and maybe others) to learn JavaScript programming, as well as SVG image editing. If people find the game fun, that's a bonus!

## Score

The score is calculated using various player statistics and metrics. It is calculated with the following operation:

- If the player used any cheat, the score is always equal to `0`.
- If the player finished the game without letting the aliens reach the ground (i.e. they died because they didn't have any life left), `20` score points are granted to them.
- For every alien killed, `0.05` points are granted to the player.
- The accuracy can grant the player up to `20` points (i.e. 20 points if accuracy = 1, 10 points if accuracy = 0.5, etc.).
- The number of aliens killed per cycle can grant the player up to `10` points (i.e. 10 points if an alien was killed every cycle, 5 points if an alien was killed every 2 cycles, etc.).

Below is an example code snippet of how to calculate the score. Please note that variable names do not always correspond to the actual code in `lib/*.js`:

```js
/**
 The accuracy is the number of aliens killed per shot fired by the player.
 It varies between 0-1

 Examples values:
   1 = every shot fired by the player killed an alien
   0.5 = half the shots fired by the player killed an alien
   0 = the player never killed any alien
 */
accuracy = totalAliensKilled / totalPlayerShots;

/**
 This is the number of aliens killed per cycle.
 It varies is between 0-1

 Example values:
   1 = the player killed an alien every cycle (almost impossible)
   0.5 = the player killed an alien every 2 cycles
   0 = the player never killed any alien
*/
killsPerCycle = totalAliensKilled / totalCyclesSurvived;

/**
 This is the final player score.
 It varies between 0-infinite positive value.
 */
score =
  (20 * (playerLives === 0) +
    20 * accuracy +
    0.05 * totalAliensKilled +
    10 * killsPerCycle) *
  (cheatUsed === false);
```

## FAQ

- **Q**: How is the score calculated at the end of the game?
- **A**: Please consult the `Score` section of the README.

- **Q**: Why doesn't the arena adapt to my browser window dimensions?
- **A**: I just didn't have time to implement it yet. Feel free to open a pull request if you have the skills to do so.

## Hackers

Since the new version of this game has a nice JavaScript API, hacking/modding should be fairly easy (for people who have some JavaScript knowledge at least). At the time, no documentation is available for the API, but every part of the code is documented. I plan to add a documentation in the near future, but my time is very scarce and I usually get very lazy when it comes to writing documentation (yes, I know, that's bad).

## Credits

All images used by the game are my own, and I made them using [Inkscape](https://inkscape.org/). The only exceptions are the icons displayed in the `Player Information` dialog, that are made by [Font Awesome](https://fontawesome.com/).

## License

```
Space Invectors - A "Space Invaders-like" game written in JavaScript, HTML & CSS, with SVG icons.
Copyright (C) 2022-2025 Twann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```
